#include <iostream>
#include <string>
#include "Store.h"
#include "Weapon.h"
#include "Armor.h"
#include "Player.h"
#include "Game.h"

using namespace std;

Store mainStore;

//Initialize what is available in Store.
Store::Store(void)
{
	storeWeapons[0].setName("Claymore");
	storeWeapons[0].setCost(500);
	storeWeapons[0].setAttackPower(40);
	storeWeapons[1].setName("Clarent");
	storeWeapons[1].setCost(750);
	storeWeapons[1].setAttackPower(50);
	storeWeapons[2].setName("Masamune");
	storeWeapons[2].setCost(1500);
	storeWeapons[2].setAttackPower(75);
	storeWeapons[3].setName("Gungnir");
	storeWeapons[3].setCost(2000);
	storeWeapons[3].setAttackPower(90);
	storeWeapons[4].setName("Ultima");
	storeWeapons[4].setCost(5000);
	storeWeapons[4].setAttackPower(125);

	storeArmor[0].setName("Chainmail");
	storeArmor[0].setCost(500);
	storeArmor[0].setDefense(30);
	storeArmor[1].setName("Platemail");
	storeArmor[1].setCost(750);
	storeArmor[1].setDefense(40);
	storeArmor[2].setName("Bandedmail");
	storeArmor[2].setCost(1500);
	storeArmor[2].setDefense(50);
	storeArmor[3].setName("Hauberk");
	storeArmor[3].setCost(2000);
	storeArmor[3].setDefense(75);
	storeArmor[4].setName("Cuirass");
	storeArmor[4].setCost(3000);
	storeArmor[4].setDefense(90);
}

Store::~Store(void)
{
}

void Store::run(){
	int itemID;


	cout << "Hello Welcome to the store, what would you like to Purchase?" << endl;
	listItems();
	cout << "Input 1-10 to purchase Item. Input '0' to leave the store." << endl;
	cout << "Gold: " << mainPlayer.returnMoney() << endl;
	cin >> itemID;

	if (itemID>5 && itemID<=10){
		purchaseArmor(itemID-6);
	}else{
		purchaseWeapon(itemID-1);
	}
	if (itemID==11){
		heal();
	}
	if (itemID!=0){
		run();
	}
	
}

//Display Items
void Store::listItems(){
	cout << "Weapons" << endl;
	for (int x=0;x<5;x++){
		cout << x+1 << " -\tName:\t" << storeWeapons[x].returnName() << endl;
		cout << " \tCost:\t" << storeWeapons[x].returnCost() << endl;
		cout << " \tDEF:\t" << storeWeapons[x].returnAttackPower() << endl;
	}
	cout << "Armor" << endl;
	for (int x=0;x<5;x++){
		cout << x+6 << " -\tName:\t" << storeArmor[x].returnName() << endl;
		cout << " \tCost:\t" << storeArmor[x].returnCost() << endl;
		cout << " \tDEF:\t" << storeArmor[x].returnDefense() << endl;
	}
	cout << "11 -\tHeal" << endl;
}

//Handle weapon purchasing 
void Store::purchaseWeapon(int itemID){
	int moneyAmount = mainPlayer.returnMoney();
	int costOfItem = storeWeapons[itemID].returnCost();
	int newMoneyAmount = moneyAmount - costOfItem;
	string newName = storeWeapons[itemID].returnName();
	int newAttack = storeWeapons[itemID].returnAttackPower();

	if (moneyAmount >= costOfItem){
		mainWeapon.setName(newName);
		mainWeapon.setAttackPower(newAttack);
		mainPlayer.setMoney(newMoneyAmount);
	}
}

//Handle Armor purchasing 
void Store::purchaseArmor(int itemID){
	int moneyAmount = mainPlayer.returnMoney();
	int costOfItem = storeArmor[itemID].returnCost();
	int newMoneyAmount = moneyAmount - costOfItem;
	string newName = storeArmor[itemID].returnName();
	int newDefense = storeArmor[itemID].returnDefense();

	if (moneyAmount >= costOfItem){
		mainArmor.setName(newName);
		mainArmor.setDefense(newDefense);
		mainPlayer.setMoney(newMoneyAmount);
	}else{
		cout << "You do not have enough gold" << endl;
	}
}

//Restore Players health
void Store::heal(){
	int moneyAmount = mainPlayer.returnMoney();
	int costOfHeal = 100;
	int baseHealth = mainPlayer.returnBaseHealth();
	int newMoneyAmount = moneyAmount - costOfHeal;
	if (moneyAmount >= costOfHeal){
		mainPlayer.setHealth(baseHealth);
	}else{
		cout << "You do not have enough gold" << endl;
	}
}
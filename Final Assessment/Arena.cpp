#include "Arena.h"
#include "Player.h"
#include "Enemy.h"
#include "Weapon.h"
#include "Armor.h"
#include "Game.h"
#include <iostream>
#include <cstdlib>
using namespace std;

Arena battleArena;



Arena::Arena(void)
{
}

Arena::~Arena(void)
{
}

void Arena::run(){
	
	//Run the battle Simulator
	cout << "Battle Number " << mainPlayer.returnEnemiesKilled() + 1 << endl;
	cout << "Enemy" << endl;
	cout << "\tName:\t" << mainEnemy.returnName() << endl;
	cout << "\tHealth:\t" << mainEnemy.returnBaseHealth() << endl;
	cout << "\tATK:\t" << mainEnemy.returnAttack() << endl;
	cout << "\tDEF:\t" << mainEnemy.returnDefense() << endl;
	
	mainGame.viewCharacter();

	system("PAUSE");
	//While both main player and enemy is alive, continue battle
	while(mainPlayer.isAlive() == true && mainEnemy.isAlive() == true){
		cout << "You Attack" << endl;
		playerAttack();
		cout << "Enemies health: " <<  mainEnemy.returnHealth() << "/" << mainEnemy.returnBaseHealth() << endl;
		system("PAUSE");
		if(mainPlayer.isAlive() == false || mainEnemy.isAlive() == false){
			break;
		}
		cout << "Enemy Attacks" << endl;
		enemyAttack();
		cout << "Your Health: " <<  mainPlayer.returnHealth() << "/" << mainPlayer.returnBaseHealth() << endl;
		system("PAUSE");
	}
	if (mainPlayer.isAlive() == true) {
		cout << "Congratulations! Enemy Defeated!" << endl;
		int newKills = mainPlayer.returnEnemiesKilled() + 1;
		mainPlayer.setEnemiesKilled(newKills);
		cout << "You earned 500 Gold!" << endl;
		int newMoney = mainPlayer.returnMoney() + 500;
		mainPlayer.setMoney(newMoney);
		mainEnemy.newEnemy();
	}else{
		cout << "You Were Defeated!" << endl;
		cout << "You earned 100 Gold for Participating" << endl;
		int newMoney = mainPlayer.returnMoney() + 100;
		mainPlayer.setMoney(newMoney);
		mainEnemy.newEnemy();
	}


}

//Handle player attacking enemy.
void Arena::playerAttack(){
	int playersAttack = mainWeapon.returnAttackPower();
	int playersDefence = mainArmor.returnDefense();
	int enemysAttack = mainEnemy.returnAttack();
	int enemysDefense = mainEnemy.returnDefense();
	double difference = playersAttack - enemysDefense;
	if (difference > 0){
		difference = difference * 2;
		int newHealth = mainEnemy.returnHealth() - difference;
		mainEnemy.setHealth(newHealth);
	}else{
		if (difference == 0){
			difference = 5;
		}else{
			difference = 0.5 * -difference;	
		}
		int newHealth = mainEnemy.returnHealth() - difference;
		mainEnemy.setHealth(newHealth);
	}

}

//Handle Enemy attacking player
void Arena::enemyAttack(){
	int playersAttack = mainWeapon.returnAttackPower();
	int playersDefence = mainArmor.returnDefense();
	int enemysAttack = mainEnemy.returnAttack();
	int enemysDefense = mainEnemy.returnDefense();
	double difference = enemysAttack - playersDefence;
	if (difference > 0){
		difference = difference * 2;
		int newHealth = mainPlayer.returnHealth() - difference;
		mainPlayer.setHealth(newHealth);
	}else{
		difference = 0.5 * -difference;
		int newHealth = mainPlayer.returnHealth() - difference;
		mainPlayer.setHealth(newHealth);
	}	
}
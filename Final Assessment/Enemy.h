#pragma once
#include "entity.h"

class Enemy :
	public Entity
{
private:
	int attack;
	int defense;
public:
	Enemy(void);
	~Enemy(void);
	void setName();
	void setAttack(int);
	void setDefense(int);
	int returnAttack();
	int returnDefense();
	void newEnemy();
};

extern Enemy mainEnemy;
#pragma once
#include "item.h"

class Weapon :
	public Item
{
private:
	int attackPower;
public:
	Weapon(void);
	~Weapon(void);
	void setAttackPower(int);
	int returnAttackPower();
	void attack(int);
	void attackDecrease(int);
};

extern Weapon mainWeapon;
extern Weapon storeWeapons[];
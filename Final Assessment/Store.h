#pragma once

class Store
{
public:
	Store(void);
	~Store(void);
	void run();
	void listItems();
	void purchaseWeapon(int);
	void purchaseArmor(int);
	void heal();
};

extern Store mainStore;
#include "Entity.h"
using namespace std;
Entity::Entity(void)
{
}

Entity::~Entity(void)
{
}

bool Entity::isAlive() {
	if (health > 0){
		return true;
	}else{
		return false;
	}
}

void Entity::setHealth(int amount){
	health = amount;
}

void Entity::setBaseHealth(int amount){
	baseHealth = amount;
}

void Entity::setName(string setName){
	name = setName;
}

int Entity::returnHealth(){
	return health;
}

int Entity::returnBaseHealth(){
	return baseHealth;
}

string Entity::returnName(){
	return name;
}

#include "Weapon.h"
#include "Random.h"

Weapon mainWeapon;
Weapon storeWeapons[5];

Weapon::Weapon(void)
{
}

Weapon::~Weapon(void)
{
}

int Weapon::returnAttackPower(){
	return attackPower;
}

void Weapon::attack(int amount){
	if (randomManager.randomInteger(10) < 3){
		attackDecrease(amount);
	}
}

void Weapon::attackDecrease(int amount){
	attackPower -= amount;
}

void Weapon::setAttackPower(int amount){
	attackPower = amount;
}


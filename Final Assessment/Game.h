#pragma once
#include <string>
using namespace std;

class Game
{
public:
	Game(void);
	~Game(void);
	void initialize();
	bool load();
	void save();
	void createNewCharacter();
	void viewCharacter();
	void mainMenu();
	bool returnYesNo(string);
	int stringToInt(string);
};

extern Game mainGame;
#pragma once
#include "item.h"

class Armor :
	public Item
{
private:
	int defense;
public:
	Armor(void);
	~Armor(void);
	void setDefense(int);
	int returnDefense();
	void takeDamage(int);
};

extern Armor mainArmor;
extern Armor storeArmor[];
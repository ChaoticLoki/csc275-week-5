#include "Item.h"

Item::Item(void)
{
}

Item::~Item(void)
{
}

void Item::setCost(int amount){
	cost = amount;
}

void Item::setName(string addName){
	name = addName;
}

int Item::returnCost(){
	return cost;
}

string Item::returnName(){
	return name;
}

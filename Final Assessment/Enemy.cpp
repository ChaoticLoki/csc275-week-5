#include "Enemy.h"
#include "Random.h"
#include "Player.h"
Enemy mainEnemy;

Enemy::Enemy(void)
{

}

Enemy::~Enemy(void)
{
}

void Enemy::setName(){
	
	int randomName;
	randomName = randomManager.randomInteger(5) + 1;

	switch (randomName){
	case 1:
		name = "Mandragora";
		break;
	case 2:
		name = "Manticore";
		break;
	case 3:
		name = "Tiamat";
		break;
	case 4:
		name = "Ted";
		break;
	case 5:
		name = "Gespenst";
		break;
	}
}

void Enemy::setAttack(int setAmount){
	attack = setAmount;
}

void Enemy::setDefense(int setAmount){
	defense = setAmount;
}

int Enemy::returnAttack(){
	return attack;
}

int Enemy::returnDefense(){
	return defense;
}

//Create a new enemy to fight based on the player's kill count.
void Enemy::newEnemy(){
	int newHealth;
	int newAttack;
	int newDefense;
	int level = mainPlayer.returnEnemiesKilled();
	int random = randomManager.randomInteger(10);
	
	if (random > 5){
		//Stronger
		newHealth = (level * randomManager.randomInteger(20)) + mainEnemy.returnBaseHealth();
		newAttack = (level * randomManager.randomInteger(20)) + mainEnemy.returnAttack();
		newDefense = (level * randomManager.randomInteger(20)) + mainEnemy.returnDefense();
	}else{
		//Weaker
		newHealth = mainEnemy.returnBaseHealth() - (level * randomManager.randomInteger(20));
		newAttack = mainEnemy.returnAttack() - (level * randomManager.randomInteger(20));
		newDefense = mainEnemy.returnDefense() - (level * randomManager.randomInteger(20));
	}
		mainEnemy.setName();
		mainEnemy.setAttack(newAttack);
		mainEnemy.setBaseHealth(newHealth);
		mainEnemy.setHealth(newHealth);
		mainEnemy.setDefense(newDefense);
}
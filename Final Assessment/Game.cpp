#include <iostream>
#include <fstream>
#include <string>

#include "Game.h"
#include "Player.h"
#include "Enemy.h"
#include "Weapon.h"
#include "Armor.h"
#include "Store.h"
#include "Arena.h"

using namespace std;

Game mainGame;

bool loadCharacter;
bool continueGame = false;

Game::Game(void)
{
}

Game::~Game(void)
{
}

void Game::initialize(){
	
	loadCharacter = returnYesNo("Do you want to load a character? (Y/N)");

	if (loadCharacter == true){
		if (load())
		{
			viewCharacter();
			system("PAUSE");
			mainMenu();
		}
		else
		{
			cout << "Error: File not found" << endl;
			createNewCharacter();
			system("PAUSE");
			battleArena.run();
			system("PAUSE");
			mainMenu();
		}
	}else{
		createNewCharacter();
		system("PAUSE");
		battleArena.run();
		system("PAUSE");
		mainMenu();
	}

}

void Game::createNewCharacter(){
	string newPlayerName;
	//Set default player stats
	mainPlayer.setBaseHealth(500);
	mainPlayer.setHealth(500);
	mainPlayer.setMoney(1000);
	mainPlayer.setEnemiesKilled(0);
	cout << "What is your name?" << endl;
	cin >> newPlayerName;
	mainPlayer.setName(newPlayerName);
	//Set Default Weapon
	mainWeapon.setName("Sword");
	mainWeapon.setAttackPower(45);
	//Set Default Armor
	mainArmor.setName("Shield");
	mainArmor.setDefense(20);
	
	//Set First Enemy Stats
	mainEnemy.setName();
	mainEnemy.setHealth(300);
	mainEnemy.setBaseHealth(300);
	mainEnemy.setAttack(30);
	mainEnemy.setDefense(10);
	viewCharacter();

	
}

void Game::viewCharacter(){
	cout << "Character:" << endl;
	cout << "\tName:\t" << mainPlayer.returnName() << endl;
	cout << "\tHealth:\t" << mainPlayer.returnHealth() << "/" << mainPlayer.returnBaseHealth() << endl;
	cout << "\tMoney:\t" << mainPlayer.returnMoney() << " Gold" << endl;
	cout << "\tKilled:\t" << mainPlayer.returnEnemiesKilled() <<" Enemies" << endl;
	cout << "Weapon:" << endl;
	cout << "\tName:\t" << mainWeapon.returnName() << endl;
	cout << "\tATK:\t" << mainWeapon.returnAttackPower() << endl;
	cout << "Armor:" << endl;
	cout << "\tName:\t" << mainArmor.returnName() << endl;
	cout << "\tDEF:\t" << mainArmor.returnDefense() << endl;
}

//Main Menu Loop
void Game::mainMenu(){
	int userInput;
	bool exitbool;
	do{
	cout << "What would you like to do? (Type in 1,2,3 or 4)" << endl << endl;
	cout << "1 - Go to arena to battle an enemy and earn gold" << endl;
	cout << "2 - Go to store to purchase new Weapons and Armor" << endl;
	cout << "3 - View Player Stats" << endl;
	cout << "4 - Save your progress" << endl;
	cout << "5 - Exit game" << endl;
	cin >> userInput;
	
		switch (userInput){
		case 1:
			cout << "Going to Arena" << endl;
			battleArena.run();
			system("PAUSE");
			break;
		case 2:
			cout << "Going to store" << endl;
			mainStore.run();
			system("PAUSE");
			break;
		case 3:
			viewCharacter();
			system("PAUSE");
			break;
		case 4:
			cout << "Saving progress" << endl;
			save();
			system("PAUSE");
			break;
		case 5:
			exitbool = returnYesNo("Are you sure? All unsaved progress will be lost");
			if (exitbool == true){
				exit(0);
			}
			break;
		default:
			cout << "Error invalid input, please try again."<< endl;
			cin.clear();
			cin.ignore(100,'\n');
			break;
		}
	}while(1);
}

bool Game::returnYesNo(string message){
	char character;
	bool boolean;
	do {
		cout << message << endl;
		cin >> character;
	} while (!strchr("yn" , tolower(character)));
	boolean = (tolower(character) == 'y');
	return boolean;
}

//Save current progress
void Game::save(){
	string savePlayerName = mainPlayer.returnName();
	int saveBaseHealth = mainPlayer.returnBaseHealth();
	int saveHealth = mainPlayer.returnHealth();
	int saveKilled = mainPlayer.returnEnemiesKilled();
	int saveMoney = mainPlayer.returnMoney();

	string saveWeaponName = mainWeapon.returnName();
	int saveAttack = mainWeapon.returnAttackPower();

	string saveArmorName = mainArmor.returnName();
	int saveDefense = mainArmor.returnDefense();

	ofstream saveFile;
	saveFile.open("save.dat");
	saveFile << savePlayerName << '\n' << saveBaseHealth << '\n' << saveHealth << '\n' << saveKilled << '\n' << saveMoney << '\n' << saveWeaponName << '\n' << saveAttack << '\n' << saveArmorName << '\n' << saveDefense << endl;
	saveFile.close();
}

//Load previous saved data
bool Game::load(){
	string data[9];
	ifstream saveFile("save.dat");
	int x = 0;
	string input;
	if (saveFile.is_open()){
		while (saveFile >> input){
			data[x] = input;
			x++;
		}
		saveFile.close();

		string savePlayerName = data[0];
		int saveBaseHealth = stringToInt(data[1]);
		int saveHealth = stringToInt(data[2]);
		int saveKilled = stringToInt(data[3]);
		int saveMoney = stringToInt(data[4]);

		string saveWeaponName = data[5];;
		int saveAttack = stringToInt(data[6]);

		string saveArmorName = data[7];
		int saveDefense = stringToInt(data[8]);

		mainPlayer.setName(savePlayerName);
		mainPlayer.setBaseHealth(saveBaseHealth);
		mainPlayer.setHealth(saveHealth);
		mainPlayer.setEnemiesKilled(saveKilled);
		mainPlayer.setMoney(saveMoney);

		mainWeapon.setName(saveWeaponName);
		mainWeapon.setAttackPower(saveAttack);

		mainArmor.setName(saveArmorName);
		mainArmor.setDefense(saveDefense);
		return true;
	}
	else
	{
		return false;
	}
}


//Convert string to integer
int Game::stringToInt(string convert){
	int intReturn;
	intReturn = atoi(convert.c_str());
	return(intReturn);
}
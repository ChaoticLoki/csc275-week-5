#pragma once
#include <string>
using namespace std;

class Item
{
private:
	string name;
	int cost;
public:
	Item(void);
	~Item(void);

	void setName(string);
	void setCost(int);
	int returnCost();
	string returnName();
};

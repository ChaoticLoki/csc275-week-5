#pragma once

class Arena
{
public:
	Arena(void);
	~Arena(void);
	void run();
	void playerAttack();
	void enemyAttack();
};

extern Arena battleArena;
#pragma once
#include "entity.h"

class Player :
	public Entity
{
private:
	int money;
	int enemiesKilled;
public:
	Player(void);
	~Player(void);
	void setMoney(int);
	int returnMoney();
	void setEnemiesKilled(int);
	int returnEnemiesKilled();
};

extern Player mainPlayer;

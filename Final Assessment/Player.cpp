#include "Player.h"

Player mainPlayer;

Player::Player(void)
{
}

Player::~Player(void)
{
}

void Player::setMoney(int amount){
	money = amount;
}

int Player::returnMoney(){
	return money;
}

void Player::setEnemiesKilled(int kills){
	enemiesKilled = kills;
}

int Player::returnEnemiesKilled(){
	return enemiesKilled;
}
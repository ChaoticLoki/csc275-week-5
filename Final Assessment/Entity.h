#pragma once
#include <string>
using namespace std;

//Base unit class that Player and Enemy inherit from
class Entity
{
protected:
	string name;
	int health;
	int baseHealth;
public:
	Entity(void);
	~Entity(void);
	void setName (string);
	void setHealth (int);
	void setBaseHealth(int);
	string returnName();
	int returnHealth();
	int returnBaseHealth();
	bool isAlive();
};